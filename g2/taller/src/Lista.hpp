#include "Lista.h"

Lista::Lista() : _primero(nullptr), _ultimo(nullptr), _longuitud(0){
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    // Completar
    Nodo* nodoActual = _primero;
    while(nodoActual!=_ultimo){
        nodoActual=nodoActual->siguiente;
        delete nodoActual->anterior;
    }
    delete nodoActual;
}

Lista& Lista::operator=(const Lista& aCopiar) {
    while(this->longitud()>0){
        this->eliminar(0);
    }
    for(int i=0; i<aCopiar.longitud(); i++){
        this->agregarAtras(aCopiar.iesimo(i));
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    // Completar
    Nodo* nuevoNodo = new Nodo(elem);
    if (longitud()>0){
        nuevoNodo->siguiente=_primero;
        _primero->anterior=nuevoNodo;
        _primero=nuevoNodo;
    }
    else{
        _primero=nuevoNodo;
        _ultimo=nuevoNodo;
    }
    _longuitud+=1;
}

void Lista::agregarAtras(const int& elem) {
    // Completar
    Nodo* nuevoNodo = new Nodo(elem);
    if(longitud()>0){
        nuevoNodo->anterior=_ultimo;
        _ultimo->siguiente=nuevoNodo;
        _ultimo=nuevoNodo;
    }
    else{
        _primero=nuevoNodo;
        _ultimo=nuevoNodo;
    }
    _longuitud+=1;
}

void Lista::eliminar(Nat i) {
    // Completar
    Nodo* nodoActual = _primero;
    int contador = 1;
    bool elimino = false;
    if (longitud()>1){
        if(i==0){
            _primero=nodoActual->siguiente;
            delete nodoActual;
            elimino=true;
        }
        if (!elimino)
            nodoActual=nodoActual->siguiente;
        while(nodoActual!=_ultimo && !elimino){
            if (contador==i){
                (nodoActual->anterior)->siguiente=nodoActual->siguiente;
                (nodoActual->siguiente)->anterior=nodoActual->anterior;
                delete nodoActual;
                elimino=true;
            }
            else{
                nodoActual=nodoActual->siguiente;
                contador++;
            }
        }
        if(i== longitud()-1 && i!=0 && !elimino){
            _ultimo=nodoActual->anterior;
            delete nodoActual;
        }
    }
    else{
        _primero= nullptr;
        _ultimo= nullptr;
        delete nodoActual;
    }
    _longuitud--;
}

int Lista::longitud() const {
    return _longuitud;
}

const int& Lista::iesimo(Nat i) const {
    // Completar
    Nodo* nodoActual = _primero;
    int contador =0;
    while (nodoActual!=_ultimo){
        if(contador==i)
            return nodoActual->valor;
        else{
            contador++;
            nodoActual=nodoActual->siguiente;
        }
    }
    if(i==_longuitud-1){
        return nodoActual->valor;
    }
}

int& Lista::iesimo(Nat i) {
    // Completar (hint: es igual a la anterior...)
    Nodo* nodoActual = _primero;
    int contador =0;
    if (i==0)
        return nodoActual->valor;
    else{
        while (nodoActual!=_ultimo){
            if(contador==i) {
                return nodoActual->valor;
            }
            else{
                contador++;
                nodoActual=nodoActual->siguiente;
            }
        }
        if(i==_longuitud-1){
            return nodoActual->valor;
        }
    }
}

void Lista::mostrar(ostream& o) {
    // Completar
}

Lista::Nodo::Nodo(const int &elem) : valor(elem), siguiente(nullptr), anterior(nullptr){}