#include <iostream>
#include <list>
using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Clase Fecha
class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes() const; //Tuve que hacer esto para que me deje definir < en fecha
    int dia() const;
    #if EJ >= 9
    bool operator==(Fecha o);
    #endif
    bool operator<(const Fecha& o) const;
    void incrementar_dia();

  private:
    uint mes_;
    uint dia_;
};

Fecha::Fecha(int mes, int dia) : mes_(mes), dia_(dia) {}

int Fecha::mes() const {
    return mes_;
}

int Fecha::dia() const {
    return dia_;
}


#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia() && this->mes() == o.mes();
    return igual_dia;
}
#endif

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}

// Ejercicio 10
void Fecha::incrementar_dia(){
    if (this->dia() == dias_en_mes(this->mes())){
        this->dia_=1;
        if (this->mes_<12)
            this->mes_+=1;
        else
            this->mes_=1;
    }
    else{
        this->dia_+=1;
    }
}

bool Fecha::operator<(const Fecha& o) const{
    bool menor_fecha = this->mes() < o.mes() || (this->mes() == o.mes() && this->dia() < o.dia());
    return menor_fecha;
}

// Ejercicio 11
class Horario {
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();

    bool operator==(Horario o);
    bool operator<(Horario h);

private:
    uint hora_;
    uint min_;

};

Horario::Horario(uint hora, uint min) : hora_(hora), min_(min){}

uint Horario::hora() {
    return hora_;
}

uint Horario::min() {
    return min_;
}

bool Horario::operator==(Horario o) {
    bool igual_hora = this->hora() == o.hora() && this->min() == o.min();
    return igual_hora;
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}

// Ejercicio 12
bool Horario::operator<(Horario h) {
    return this->hora()< h.hora()|| (this->hora() == h.hora() && this->min() < h.min());
}

// Ejercicio 13
class Recordatorio {
public:
    Recordatorio(Fecha fecha, Horario horario, string mensaje);
    string mensaje();
    Fecha fecha();
    Horario horario();
    bool operator<(Recordatorio r);
private:
    string mensaje_;
    Fecha fecha_;
    Horario horario_;


};

Recordatorio::Recordatorio(Fecha fecha, Horario horario, string mensaje) : fecha_(fecha), horario_(horario), mensaje_(mensaje){}

string Recordatorio::mensaje() {
    return mensaje_;
}

Fecha Recordatorio::fecha() {
    return fecha_;
}

Horario Recordatorio::horario() {
    return horario_;
}

bool Recordatorio::operator<(Recordatorio r) {
    return this->fecha() < r.fecha() || (this->fecha() == r.fecha() && this->horario() < r.horario());
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.mensaje() << " @ " << r.fecha() << " " << r.horario();
    return os;
}

// Ejercicio 14
class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();

private:
    Fecha dia_actual_;
    map<Fecha, list<Recordatorio>> agenda_;
};

Agenda::Agenda(Fecha fecha_inicial) : dia_actual_(fecha_inicial){}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    agenda_[rec.fecha()].push_back(rec);
    //No funciona sin definir < en Fecha
}

Fecha Agenda::hoy() {
    return dia_actual_;
}

void Agenda::incrementar_dia() {
    dia_actual_.incrementar_dia();
}

list <Recordatorio> Agenda::recordatorios_de_hoy() {
    if(this->agenda_.count(hoy())==1)
        return this->agenda_[hoy()];
    else
        return {};
}

ostream& operator<<(ostream& os, Agenda a) {
    os << a.hoy() << endl;
    os << "=====" << endl;
    //Ordenar aca.
    list<Recordatorio> lr = a.recordatorios_de_hoy();
    lr.sort();
    for(Recordatorio r : lr){
        os << r << endl;
    }
    return os;
}


