#include <string>

template <class T>
Conjunto<T>::Conjunto() : _raiz(NULL), _cardinal(0) {}

template <class T>
Conjunto<T>::~Conjunto() {
    destruir(this->_raiz);
}

template <class T>
void Conjunto<T>::destruir(Nodo* x){
    if(x != NULL){
        destruir(x->der);
        destruir(x->izq);
        delete x;
    }
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo* x = this->_raiz;
    while(x != NULL && x->valor != clave){
        if (clave < x->valor)
            x = x->izq;
        else
            x = x->der;
    }
    return x != NULL;
}

template <class T>
typename Conjunto<T>::Nodo* Conjunto<T>::buscar(const T& clave) const {
    Nodo* x = this->_raiz;
    while(x != NULL && x->valor != clave){
        if (clave < x->valor)
            x = x->izq;
        else
            x = x->der;
    }
    return x;
}

template <class T>
void Conjunto<T>::insertar(const T& valor) {
    Nodo* y = NULL;
    Nodo* x = this->_raiz;
    while(x != NULL && x->valor != valor){
        y = x;
        if (valor < x->valor)
            x = x->izq;
        else
            x = x->der;
    }
    if(x == NULL){
        Nodo* z = new Nodo (valor);
        if(y == NULL)
            this->_raiz = z;
        else if(valor < y->valor){
            y->izq = z;
            z->padre = y;
        }

        else{
            y->der = z;
            z->padre = y;
        }
        _cardinal++;
    }
}

template <class T>
void Conjunto<T>::remover(const T& valor) {
    Nodo* y = NULL;
    Nodo* x = buscar(valor);

    if(x != NULL){
        _cardinal--;
        if(x->izq == NULL){
            transplant(x, x->der);
        }
        else if (x->der == NULL){
            transplant(x,x->izq);
        }
        else {
            y = buscar(minimoArbol(x->der->valor));
            if (y->padre != x){
                transplant(y, y->der);
                y->der= x->der;
                y->der->padre = y;
            }
            transplant(x,y);
            y->izq = x->izq;
            y->izq->padre =y;
        }
        delete x;
    }
    return;
}

template <class T>
void Conjunto<T>::transplant(Nodo* x, Nodo* y){
    if (x->padre == NULL)
        _raiz = y;
    else{
        if (x == x->padre->izq)
            x->padre->izq = y;
        else
            x->padre->der = y;
    }
    if (y != NULL)
        y->padre = x->padre;
}


template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* x = buscar(clave);
    Nodo* y = NULL;
    if(x->der != NULL)
        return minimoArbol(x->der->valor);
    y = x->padre;
    while(y != NULL && x == y->der){
        x = y;
        y = y->padre;
    }
    return y->valor;
}

template <class T>
const T& Conjunto<T>::minimoArbol(const T& raiz) const {
    Nodo* y = NULL;
    Nodo* x = buscar(raiz);
    while (x != NULL){
        y = x;
        x = x->izq;
    }
    return y->valor;
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* y = NULL;
    Nodo* x = this->_raiz;
    while (x != NULL){
        y = x;
        x = x->izq;
    }
    return y->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* y = NULL;
    Nodo* x = this->_raiz;
    while (x != NULL){
        y = x;
        x = x->der;
    }
    return y->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

